Why should you use Laverna as note taking application?

### No registration required
Laverna is a web app written in JavaScript which means it requires no installation and no registration.

### Encryption
Laverna can encrypt all your notes on client side with SJCL library and no one, except you, can get access to them.

### Markdown
Laverna web app uses pagedown and ACE editor. Also we support several editing modes: distraction-free mode, preview mode and normal.

### Privacy
None of us can get access to your personal data because we are using IndexedDB and localStorage. In fact all your information will be stored only on client-side.

### Synchronizing
In settings page you can enable synchronizing with cloud storage. After that all your information can be accessed from anywhere.

### Tasks
Application also supports tasks. Adding tasks as easy as in GitHub.

### Open source
And most importantly all of our code is open source and available on GitHub.

