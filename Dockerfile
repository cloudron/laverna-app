FROM cloudron/base:0.3.0
MAINTAINER Girish Ramakrishnan <girish@cloudron.io>

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir -p /app/code
WORKDIR /app/code
RUN curl -L https://github.com/Laverna/laverna/tarball/7ddbbab9676a6d69eaa543cc8f8a29fd728559b8 | tar -xz --strip-components 1 -f -

# Install project dependencies
WORKDIR /app/code
# Single run command allows us to reduce the image size
RUN npm install bower grunt \
    && npm install -g grunt-cli \
    && npm install \
    && node_modules/.bin/bower install --force --allow-root \
    && grunt build \
    && rm -rf node_modules

# Install a basic http server
RUN npm install -g serv

EXPOSE 8000

# Used by node-config to load config/docker.json
ENV NODE_ENV docker

WORKDIR /app/code/app

CMD [ "serv", "--public", "."]

